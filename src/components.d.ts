/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */


import { HTMLStencilElement, JSXBase } from '@stencil/core/internal';


export namespace Components {
  interface AppAddAssignment {
    'newAssignmentName': string;
    'newCourseId': number;
    'newDescription': string;
    'newDueDate': string;
    'newPossiblePoints': string;
  }
  interface AppAddCourse {
    'newCourseName': string;
    'newCourseNumber': string;
    'newInstructor': string;
    'newSectionNumber': string;
    'newTerm': string;
  }
  interface AppAssignment {
    'assignment': {id: number, assignmentName: string, description: string, possiblePoints: string, dueDate: string};
  }
  interface AppAssignmentsList {}
  interface AppCourse {
    'course': {id: number, courseName: string, courseNumber: string, sectionNumber: string, term: string, instructor: string};
  }
  interface AppCourseList {}
  interface AppFooter {}
  interface AppRoot {}
}

declare global {


  interface HTMLAppAddAssignmentElement extends Components.AppAddAssignment, HTMLStencilElement {}
  var HTMLAppAddAssignmentElement: {
    prototype: HTMLAppAddAssignmentElement;
    new (): HTMLAppAddAssignmentElement;
  };

  interface HTMLAppAddCourseElement extends Components.AppAddCourse, HTMLStencilElement {}
  var HTMLAppAddCourseElement: {
    prototype: HTMLAppAddCourseElement;
    new (): HTMLAppAddCourseElement;
  };

  interface HTMLAppAssignmentElement extends Components.AppAssignment, HTMLStencilElement {}
  var HTMLAppAssignmentElement: {
    prototype: HTMLAppAssignmentElement;
    new (): HTMLAppAssignmentElement;
  };

  interface HTMLAppAssignmentsListElement extends Components.AppAssignmentsList, HTMLStencilElement {}
  var HTMLAppAssignmentsListElement: {
    prototype: HTMLAppAssignmentsListElement;
    new (): HTMLAppAssignmentsListElement;
  };

  interface HTMLAppCourseElement extends Components.AppCourse, HTMLStencilElement {}
  var HTMLAppCourseElement: {
    prototype: HTMLAppCourseElement;
    new (): HTMLAppCourseElement;
  };

  interface HTMLAppCourseListElement extends Components.AppCourseList, HTMLStencilElement {}
  var HTMLAppCourseListElement: {
    prototype: HTMLAppCourseListElement;
    new (): HTMLAppCourseListElement;
  };

  interface HTMLAppFooterElement extends Components.AppFooter, HTMLStencilElement {}
  var HTMLAppFooterElement: {
    prototype: HTMLAppFooterElement;
    new (): HTMLAppFooterElement;
  };

  interface HTMLAppRootElement extends Components.AppRoot, HTMLStencilElement {}
  var HTMLAppRootElement: {
    prototype: HTMLAppRootElement;
    new (): HTMLAppRootElement;
  };
  interface HTMLElementTagNameMap {
    'app-add-assignment': HTMLAppAddAssignmentElement;
    'app-add-course': HTMLAppAddCourseElement;
    'app-assignment': HTMLAppAssignmentElement;
    'app-assignments-list': HTMLAppAssignmentsListElement;
    'app-course': HTMLAppCourseElement;
    'app-course-list': HTMLAppCourseListElement;
    'app-footer': HTMLAppFooterElement;
    'app-root': HTMLAppRootElement;
  }
}

declare namespace LocalJSX {
  interface AppAddAssignment extends JSXBase.HTMLAttributes<HTMLAppAddAssignmentElement> {
    'newAssignmentName'?: string;
    'newCourseId'?: number;
    'newDescription'?: string;
    'newDueDate'?: string;
    'newPossiblePoints'?: string;
    'onAssignmentAdded'?: (event: CustomEvent<any>) => void;
    'onFinishedAddingAssignment'?: (event: CustomEvent<any>) => void;
  }
  interface AppAddCourse extends JSXBase.HTMLAttributes<HTMLAppAddCourseElement> {
    'newCourseName'?: string;
    'newCourseNumber'?: string;
    'newInstructor'?: string;
    'newSectionNumber'?: string;
    'newTerm'?: string;
    'onCourseAdded'?: (event: CustomEvent<any>) => void;
    'onFinishedAdding'?: (event: CustomEvent<any>) => void;
  }
  interface AppAssignment extends JSXBase.HTMLAttributes<HTMLAppAssignmentElement> {
    'assignment'?: {id: number, assignmentName: string, description: string, possiblePoints: string, dueDate: string};
    'onAssignmentDeleted'?: (event: CustomEvent<any>) => void;
    'onAssignmentEdited'?: (event: CustomEvent<any>) => void;
  }
  interface AppAssignmentsList extends JSXBase.HTMLAttributes<HTMLAppAssignmentsListElement> {
    'onViewingAssignments'?: (event: CustomEvent<any>) => void;
  }
  interface AppCourse extends JSXBase.HTMLAttributes<HTMLAppCourseElement> {
    'course'?: {id: number, courseName: string, courseNumber: string, sectionNumber: string, term: string, instructor: string};
    'onCourseDeleted'?: (event: CustomEvent<any>) => void;
    'onCourseEdited'?: (event: CustomEvent<any>) => void;
  }
  interface AppCourseList extends JSXBase.HTMLAttributes<HTMLAppCourseListElement> {}
  interface AppFooter extends JSXBase.HTMLAttributes<HTMLAppFooterElement> {}
  interface AppRoot extends JSXBase.HTMLAttributes<HTMLAppRootElement> {}

  interface IntrinsicElements {
    'app-add-assignment': AppAddAssignment;
    'app-add-course': AppAddCourse;
    'app-assignment': AppAssignment;
    'app-assignments-list': AppAssignmentsList;
    'app-course': AppCourse;
    'app-course-list': AppCourseList;
    'app-footer': AppFooter;
    'app-root': AppRoot;
  }
}

export { LocalJSX as JSX };


declare module "@stencil/core" {
  export namespace JSX {
    interface IntrinsicElements extends LocalJSX.IntrinsicElements {}
  }
}


