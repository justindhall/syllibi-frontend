import {Component,
  h} from '@stencil/core';

@Component({
  tag: 'app-footer',
})
export class AppFooter {

  render() {
    return [
      <ion-footer>
        <ion-toolbar color="tertiary">
          <ion-grid>
            <ion-row>
              <ion-col>
              </ion-col>
              <ion-col>
                <a href="/"><ion-icon name="document"/></a>
              </ion-col>
              <ion-col>
                <a href="/assignments"><ion-icon name="paper"/></a>
              </ion-col>
              <ion-col>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-toolbar>
      </ion-footer>
    ]
  }
}
