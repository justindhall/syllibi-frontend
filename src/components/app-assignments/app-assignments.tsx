import {Component, h, State, Prop, Event, EventEmitter} from '@stencil/core';
import _ from 'underscore';

@Component({
  tag: 'app-assignment',
})
export class AppAssignments {

  @Prop() assignment: {id: number, assignmentName: string, description: string, possiblePoints: string, dueDate: string};

  @Event() assignmentDeleted: EventEmitter;
  @Event() assignmentEdited: EventEmitter;

  @State() editing: boolean = false;

  newAssignmentName: string;
  newDescription: string;
  newPossiblePoints: string;
  newDueDate: string;

  toggle() {
    this.editing = !this.editing
  }

  destroy() {
    this.assignmentDeleted.emit({id: this.assignment.id});
  }

  assignmentNameValueChanged(event) {
    this.newAssignmentName = event.target.value;
  }

  descriptionValueChanged(event) {
    this.newDescription = event.target.value;
  }
  possiblePointsValueChanged(event) {
    this.newPossiblePoints = event.target.value;
  }

  dueDateValueChanged(event) {
    this.newDueDate = event.target.value;
  }

  updated() {
    this.assignmentEdited.emit( {id: this.assignment.id, assignmentName: this.newAssignmentName, description: this.newDescription,
                          possiblePoints: this.newPossiblePoints, dueDate: this.newDueDate});
    this.toggle();
  }

  render() {
    if(this.editing) {
      return [
        <ion-list>
          <ion-item>
            <ion-label position="stacked">Assignment Name</ion-label>
            <ion-input placeholder={this.assignment.assignmentName} onInput={(event) => { this.assignmentNameValueChanged(event) }} />
          </ion-item>
          <ion-item>
            <ion-label position="stacked">Description</ion-label>
            <ion-input placeholder={this.assignment.description} onInput={(event) => { this.descriptionValueChanged(event) }} />
          </ion-item>
          <ion-item>
            <ion-label position="stacked">Possible Points</ion-label>
            <ion-input placeholder={this.assignment.possiblePoints} onInput={(event) => { this.possiblePointsValueChanged(event) }} />
          </ion-item>
          <ion-item>
            <ion-label position="stacked">Due Date</ion-label>
            <ion-input placeholder={this.assignment.dueDate} onInput={(event) => { this.dueDateValueChanged(event) }} />
          </ion-item>
        </ion-list>,
        <ion-item>
          <ion-button onClick={() => { this.updated()}}>
            Save Changes
          </ion-button>
        </ion-item>
      ]
    } else {
      return [
        <ion-list>
          <ion-card-header>
            {this.assignment.dueDate}
          </ion-card-header>
          <ion-item lines="none">
            <ion-text>
              {this.assignment.assignmentName}
              {this.assignment.possiblePoints}
            </ion-text>
          </ion-item>
          <ion-item>
            <ion-text>
              {this.assignment.description}
            </ion-text>
          </ion-item>
          <ion-item>
            <ion-button onClick={() => this.toggle()}>
              Edit Assignment
            </ion-button>
            <ion-button color="danger" onClick={() => this.destroy()}>
              Remove Assignment
            </ion-button>
          </ion-item>
        </ion-list>
      ]
    }
  }

}
