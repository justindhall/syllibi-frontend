import { Component, h, State, Listen } from '@stencil/core';
import _ from 'underscore';
import {CoursesHttpService} from "../../http_services/courses.http_service";

@Component({
  tag: 'app-course-list',
})
export class AppCourseList {

  @State() courseList: any[];
  @State() addingCourse: boolean = false;
  @State() viewAssignments: boolean = false;

  toggleAddCourse() {
    this.addingCourse = !this.addingCourse
  }

  toggleViewAssignments() {
    this.viewAssignments = !this.viewAssignments
  }

  async componentWillLoad() {
    const courses = await new CoursesHttpService().getRequest('http://localhost:3000/courses');
    this.courseList = courses;
  }

  async create(course) {
    const response = await new CoursesHttpService().create({courseName: course.courseName, courseNumber: course.courseNumber,
          sectionNumber: course.sectionNumber, term: course.term, instructor: course.instructor});
    this.courseList = [...this.courseList, response]
  }

  async delete(id) {
    await new CoursesHttpService().delete(id);
    const foundCourse = _.findWhere(this.courseList, {id: id});
    const index = _.indexOf(this.courseList, foundCourse);
    const copy = [...this.courseList];
    delete copy[index];
    this.courseList = copy;
  }

  async update(course) {
    const foundCourse = _.findWhere(this.courseList, {id: course.id});
    const index = _.indexOf(this.courseList, foundCourse);
    const response = await new CoursesHttpService().update(course);
    const copy = [...this.courseList];
    copy[index] = response;
    this.courseList = copy;
  }

  async where(params: any) {
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    const fullUrl = `assignments?${queryString}`;
    return await new CoursesHttpService().getRequest(fullUrl);
  }

  @Listen('courseAdded')
  add(event) {
    this.create(event.detail);
  }

  @Listen('courseDeleted')
  destroy(event) {
    this.delete(event.detail.id);
  }

  @Listen('courseEdited')
  edit(event) {
    this.update(event.detail)
  }

  @Listen('finishedAdding')
  flipCourseToggle() {
    this.toggleAddCourse();
  }

  @Listen('viewingAssignments')
  flipAssignmentToggle() {
    this.toggleViewAssignments()
  }

  renderCourseList() {
    return this.courseList.map((course) => {
      return [
        <ion-card>
          <app-course course={course} />
        </ion-card>
      ]
    })
  }

  render() {
    if (this.addingCourse) {
      return [
        <app-add-course />
      ]
    } else if (this.viewAssignments) {
      return [
        <ion-header>
          <ion-toolbar>
            <ion-title padding-top>
              My Assignments
            </ion-title>
          </ion-toolbar>
        </ion-header>,
        <ion-content>
          <app-assignments-list />
        </ion-content>,
        <ion-footer>
          <ion-toolbar color="tertiary">
            <ion-grid>
              <ion-row>
                <ion-col>
                </ion-col>
                <ion-col>
                  <a href="#" onClick={() => this.toggleViewAssignments()}>
                    <ion-icon name="document"/>
                  </a>
                </ion-col>
                <ion-col>
                  <a href="#">
                    <ion-icon name="paper"/>
                  </a>
                </ion-col>
                <ion-col>
                </ion-col>
              </ion-row>
            </ion-grid>
          </ion-toolbar>
        </ion-footer>
      ]
    } else if (this.courseList.length === 0) {
      return [
        <ion-header>
          <ion-toolbar>
            <ion-title padding-top>
              My Courses
            </ion-title>
          </ion-toolbar>
        </ion-header>,
        <ion-content>
          <a href="#" onClick={() => this.toggleAddCourse()}><img src="../../assets/icon/illo_AddCourse.svg" /></a><br/>
          <div class="ion-text-center">
            <ion-text text-center>Add a course</ion-text><br/>
            <ion-text text-center>to upload a syllabus</ion-text>
          </div>
        </ion-content>,
        <ion-footer>
          <ion-toolbar color="tertiary">
            <ion-grid>
              <ion-row>
                <ion-col>
                </ion-col>
                <ion-col>
                  <a href="/">
                    <ion-icon name="document"/>
                  </a>
                </ion-col>
                <ion-col>
                  <a href="#" onClick={() => this.toggleViewAssignments()}>
                    <ion-icon name="paper"/>
                  </a>
                </ion-col>
                <ion-col>
                </ion-col>
              </ion-row>
            </ion-grid>
          </ion-toolbar>
        </ion-footer>
      ]
    } else {
      return [
        <ion-header>
          <ion-toolbar>
            <ion-title padding-top>
              My Courses
            </ion-title>
          </ion-toolbar>
        </ion-header>,
        <ion-content>
          <ion-list>
            {this.renderCourseList()}
          </ion-list>
          <ion-item lines="none">
            <a href="#" onClick={() => this.toggleAddCourse()}><ion-img src="../../assets/icon/AddCourse.svg"/></a>
          </ion-item>
        </ion-content>,
        <ion-footer>
          <ion-toolbar color="tertiary">
            <ion-grid>
              <ion-row>
                <ion-col>
                </ion-col>
                <ion-col>
                  <a href="#"><ion-icon name="document"/></a>
                </ion-col>
                <ion-col>
                  <a href="#" onClick={() => this.toggleViewAssignments()}><ion-icon name="paper"/></a>
                </ion-col>
                <ion-col>
                </ion-col>
              </ion-row>
            </ion-grid>
          </ion-toolbar>
        </ion-footer>
      ];
    }
  }
}
