import {Component,
  h,
  Prop,
  EventEmitter,
  Event} from '@stencil/core';
import _ from 'underscore';

@Component({
  tag: 'app-add-course',
})
export class AppAddCourse {

  @Prop() newCourseName: string;
  @Prop() newCourseNumber: string;
  @Prop() newSectionNumber: string;
  @Prop() newTerm: string;
  @Prop() newInstructor: string;

  @Event() courseAdded: EventEmitter;
  @Event() finishedAdding: EventEmitter;

  courseNameValueChanged(event) {
    this.newCourseName = event.target.value;
  }

  courseNumberValueChanged(event) {
    this.newCourseNumber = event.target.value;
  }
  sectionNumberValueChanged(event) {
    this.newSectionNumber = event.target.value;
  }

  termValueChanged(event) {
    this.newTerm = event.target.value;
  }

  instructorValueChanged(event) {
    this.newInstructor = event.target.value;
  }

  resetFields() {
    this.newCourseName = '';
    this.newCourseNumber = '';
    this.newSectionNumber = '';
    this.newTerm = '';
    this.newInstructor = '';
  }

  addCourse() {
    this.courseAdded.emit( {courseName: this.newCourseName, courseNumber: this.newCourseNumber, sectionNumber: this.newSectionNumber,
                          term: this.newTerm, instructor: this.newInstructor});
    this.resetFields();
    this.finished()
  }

  finished() {
    this.finishedAdding.emit()
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>
            Add Course
          </ion-title>
          <button slot="end" onClick={() => this.finished()}>
            Cancel
          </button>
        </ion-toolbar>
      </ion-header>,
      <ion-card>
        <ion-card-content>
          <ion-list>
            <ion-item>
              <ion-label position="stacked">
                Course Name
              </ion-label>
              <ion-input value={this.newCourseName} onInput={(event) => { this.courseNameValueChanged(event) }} />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">
                Course Number
              </ion-label>
              <ion-input value={this.newCourseNumber} onInput={(event) => { this.courseNumberValueChanged(event) }} />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">
                Section Number
              </ion-label>
              <ion-input value={this.newSectionNumber} onInput={(event) => { this.sectionNumberValueChanged(event) }} />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">
                Term
              </ion-label>
              <ion-input value={this.newTerm} onInput={(event) => { this.termValueChanged(event) }} />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">
                Instructor
              </ion-label>
              <ion-input value={this.newInstructor} onInput={(event) => { this.instructorValueChanged(event) }} />
            </ion-item>
            <br/>
            <br/>
            <br/>
            <ion-button expand="full" onClick={() => this.addCourse()}>
              Save Changes
            </ion-button>
            <ion-button fill="clear" expand="full" onClick={() => this.addCourse()}>
              Delete Course
            </ion-button>
          </ion-list>
        </ion-card-content>
      </ion-card>
    ]
  }

}
