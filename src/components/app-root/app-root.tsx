import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-root',
})
export class AppRoot {

  render() {
    return (
      <ion-app>
        <ion-router useHash={false}>
          <ion-route url="/" component="app-course-list" />
          <ion-route url="/addCourse" component="app-add-course" />
          <ion-route url="/assignments" component="app-assignments-list" />
          <ion-route url="/addAssignment" component="app-add-assignment" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
