import { Component, h, State, Listen, Event, EventEmitter } from '@stencil/core';
import _ from 'underscore';
import {AssignmentsHttpService} from "../../http_services/assignments.http_service";

@Component({
  tag: 'app-assignments-list',
})
export class AppAssignmentsList {

  @State() assignmentList: any[];
  @State() adding: boolean = false;

  @Event() viewingAssignments: EventEmitter;

  toggle() {
    this.adding = !this.adding
  }

  async componentWillLoad() {
    const assignments = await new AssignmentsHttpService().getRequest('http://localhost:3000/assignments');
    this.assignmentList = assignments;
  }

  async create(assignment) {
    const response = await new AssignmentsHttpService().create({assignmentName: assignment.assignmentName, description: assignment.description,
                    possiblePoints: assignment.possiblePoints, dueDate: assignment.dueDate, course_id: assignment.course_id});
    this.assignmentList = [...this.assignmentList, response]
    console.log(assignment);
  }

  async delete(id) {
    await new AssignmentsHttpService().delete(id);
    const foundAssignment = _.findWhere(this.assignmentList, {id: id});
    const index = _.indexOf(this.assignmentList, foundAssignment);
    const copy = [...this.assignmentList];
    delete copy[index];
    this.assignmentList = copy;
  }

  async update(assignment) {
    const foundassignment = _.findWhere(this.assignmentList, {id: assignment.id});
    const index = _.indexOf(this.assignmentList, foundassignment);
    const response = await new AssignmentsHttpService().update(assignment);
    const copy = [...this.assignmentList];
    copy[index] = response;
    this.assignmentList = copy;
  }

  finishedViewing() {
    this.viewingAssignments.emit()
  }

  @Listen('assignmentAdded')
  add(event) {
    console.log("heard ya")
    this.create(event.detail);
  }

  @Listen('assignmentDeleted')
  destroy(event) {
    this.delete(event.detail.id);
  }

  @Listen('assignmentEdited')
  edit(event) {
    this.update(event.detail)
  }

  @Listen('finishedAddingAssignment')
  flipToggle() {
    this.toggle();
  }

  renderAssignmentList() {
    return this.assignmentList.map((assignment) => {
      return [
        <ion-card>
          <app-assignment assignment={assignment} />
        </ion-card>
      ]
    })
  }

  render() {
    if (this.adding) {
      return [
        <app-add-assignment />
      ]
    }
    else if (this.assignmentList.length === 0) {
      return [
          <div text-align-center>
            <a href="#" onClick={() => this.toggle()}><img src="../../assets/icon/illo_AddCourse.svg" /></a><br/>
            <ion-text>Our robots are working hard</ion-text><br/>
            <ion-text>uploading other assignments.</ion-text>
            <ion-text>Would you like to manually</ion-text>
            <ion-text>upload them?</ion-text>
          </div>
      ]
    } else {
      return [
        <ion-list>
          {this.renderAssignmentList()}
        </ion-list>,
        <a href="#" onClick={() => this.toggle()}><img src="../../assets/icon/AddCourse.svg"/></a>
      ];
    }
  }
}
