import {Component,
  h,
  Prop,
  EventEmitter,
  Event} from '@stencil/core';
import _ from 'underscore';

@Component({
  tag: 'app-add-assignment',
})
export class AppAddAssignment {

  @Prop() newAssignmentName: string;
  @Prop() newDescription: string;
  @Prop() newPossiblePoints: string;
  @Prop() newDueDate: string;
  @Prop() newCourseId: number;

  @Event() assignmentAdded: EventEmitter;
  @Event() finishedAddingAssignment: EventEmitter;

  courseNameValueChanged(event) {
    this.newAssignmentName = event.target.value;
  }

  courseNumberValueChanged(event) {
    this.newDescription = event.target.value;
  }
  sectionNumberValueChanged(event) {
    this.newPossiblePoints = event.target.value;
  }

  termValueChanged(event) {
    debugger;
    this.newDueDate = event.target.value;
  }

  courseIdValueChanged(event) {
    this.newCourseId = event.target.value;
  }

  resetFields() {
    this.newAssignmentName = '';
    this.newDescription = '';
    this.newPossiblePoints = '';
    this.newDueDate = '';
    this.newCourseId = null;
  }

  addAssignment() {
    this.assignmentAdded.emit( {assignmentName: this.newAssignmentName, description: this.newDescription, possiblePoints: this.newPossiblePoints,
                          dueDate: this.newDueDate, course_id: this.newCourseId});
    this.resetFields();
    this.finished();
  }

  finished() {
    this.finishedAddingAssignment.emit()
  }

  render() {
    return [
      <ion-card>
        <ion-card-content>
          <ion-list>
            <ion-item>
              <ion-label position="stacked">
                Assignment Name
              </ion-label>
              <ion-input value={this.newAssignmentName} onInput={(event) => { this.courseNameValueChanged(event) }} />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">Due Date</ion-label>
              <ion-datetime display-format="MM DD YY" value={this.newDueDate} onInput={(event) => { this.termValueChanged(event) }}></ion-datetime>
            </ion-item>
            <ion-item> 
              <ion-label position="stacked">
                Description
              </ion-label>
              <ion-input value={this.newDescription} onInput={(event) => { this.courseNumberValueChanged(event) }} />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">
                Possible Points
              </ion-label>
              <ion-input value={this.newPossiblePoints} onInput={(event) => { this.sectionNumberValueChanged(event) }} />
            </ion-item>
            <ion-item>
              <ion-label position="stacked">
                Course ID
              </ion-label>
              <ion-input onInput={(event) => { this.courseIdValueChanged(event) }} />
            </ion-item>
            <br/>
            <br/>
            <br/>
            <ion-button expand="full" onClick={() => this.addAssignment()}>
              Create Assignment
            </ion-button>
          </ion-list>
        </ion-card-content>
      </ion-card>
    ]
  }

}
