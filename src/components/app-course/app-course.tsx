import {Component, h, State, Prop, Event, EventEmitter} from '@stencil/core';
import _ from 'underscore';

@Component({
  tag: 'app-course',
})
export class AppCourse {

  @Prop() course: {id: number, courseName: string, courseNumber: string, sectionNumber: string, term: string, instructor: string}

  @Event() courseDeleted: EventEmitter;
  @Event() courseEdited: EventEmitter;

  @State() editing: boolean = false;

  newCourseName: string;
  newCourseNumber: string;
  newSectionNumber: string;
  newTerm: string;
  newInstructor: string;
  assignmentCount: number;

  toggle() {
    this.editing = !this.editing
  }

  destroy() {
    this.courseDeleted.emit({id: this.course.id});
  }

  courseNameValueChanged(event) {
    this.newCourseName = event.target.value;
  }

  courseNumberValueChanged(event) {
    this.newCourseNumber = event.target.value;
  }
  sectionNumberValueChanged(event) {
    this.newSectionNumber = event.target.value;
  }

  termValueChanged(event) {
    this.newTerm = event.target.value;
  }

  instructorValueChanged(event) {
    this.newInstructor = event.target.value;
  }

  updated() {
    this.courseEdited.emit( {id: this.course.id, courseName: this.newCourseName, courseNumber: this.newCourseNumber,
                          sectionNumber: this.newSectionNumber, term: this.newTerm, instructor: this.newInstructor});
    this.toggle();
  }

  render() {
    if(this.editing) {
      return [
        <ion-list>
          <ion-item>
            <ion-label position="stacked">Course Name</ion-label>
            <ion-input placeholder={this.course.courseName} onInput={(event) => { this.courseNameValueChanged(event) }} />
          </ion-item>
          <ion-item>
            <ion-label position="stacked">Course Number</ion-label>
            <ion-input placeholder={this.course.courseNumber} onInput={(event) => { this.courseNumberValueChanged(event) }} />
          </ion-item>
          <ion-item>
            <ion-label position="stacked">Section Number</ion-label>
            <ion-input placeholder={this.course.sectionNumber} onInput={(event) => { this.sectionNumberValueChanged(event) }} />
          </ion-item>
          <ion-item>
            <ion-label position="stacked">Term</ion-label>
            <ion-input placeholder={this.course.term} onInput={(event) => { this.termValueChanged(event) }} />
          </ion-item>
          <ion-item>
            <ion-label position="stacked">Instructor</ion-label>
            <ion-input placeholder={this.course.instructor} onInput={(event) => { this.instructorValueChanged(event) }} />
          </ion-item>
        </ion-list>,
        <ion-item>
          <ion-button onClick={() => { this.updated()}}>
            Save Changes
          </ion-button>
        </ion-item>
      ]
    } else {
      return [
        <ion-list>
          <ion-card-header>
            {this.course.courseName}
          </ion-card-header>
          <ion-item lines="none">
            <ion-text>
              {this.course.courseNumber} - {this.course.sectionNumber}
            </ion-text>
          </ion-item>
          <ion-item>
            <ion-text>
              {this.course.instructor}
            </ion-text>
          </ion-item>
          <ion-item>
            <ion-icon name="paper" />
            <ion-text>Assignments</ion-text>
            <ion-text>{this.assignmentCount}</ion-text>
            <ion-icon slot="end" name="add-circle-outline" />
          </ion-item>
          <ion-item>
            <ion-button onClick={() => this.toggle()}>
              Edit Course
            </ion-button>
            <ion-button color="danger" onClick={() => this.destroy()}>
              Remove Course
            </ion-button>
          </ion-item>
        </ion-list>
      ]
    }
  }
}
